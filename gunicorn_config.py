command = '/home/xmax/code/web/venv/bin/gunicorn'
pythonpath = '/home/www/code/web'
bind = '127.0.0.1:8000'
workers = 3
user = 'xmax'
limit_request_fields = 32000
limit_request_field_size = 0
raw_env = 'DJANGO_SETTINGS_MODULE=com_train.settings'

