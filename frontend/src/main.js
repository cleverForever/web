// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Vuetify from 'vuetify'
import axios from 'axios'
import VueAxios from 'vue-axios'

import 'vuetify/dist/vuetify.min.css'

const opts = {
  theme: {
    accent: '#5abeeb',
    light_accent: '#c8e9fc',
    success: '#8fc738',
    primary: '#5abeeb',
    warning: '#ffa642',
    error: '#d84f3d'
  }
}

Vue.config.productionTip = false

/* eslint-disable no-new */
Vue.use(Vuetify)

Vue.use(VueAxios)
Vue.prototype.$axios = axios

new Vue({
  vuetify: new Vuetify(opts),
  el: '#app',
  router: router,
  components: { App },
  template: '<App/>',
})
