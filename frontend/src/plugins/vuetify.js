// src/plugins/vuetify.js

import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

Vue.use(Vuetify)

const opts = {
  theme: {
    accent: '#5abeeb',
    light_accent: '#c8e9fc',
    success: '#8fc738',
    primary: '#5abeeb',
    warning: '#ffa642',
    error: '#d84f3d'
  }
}

export default new Vuetify(opts)
