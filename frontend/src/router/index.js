import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Details from '@/components/Details'
import PageNotFound from '@/components/PageNotFound'

Vue.use(Router)

const routes = [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/order/:slug/',
      name: 'Details',
      component: Details
    },
    { path: "*", component: PageNotFound }
]

const router = new Router({
    routes,
    el: '#container_detail'
})

export default router
