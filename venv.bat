venv\Scripts\python.exe -m pip install -r requirements\requirements.txt

@echo Off
echo %~dp0src
rmdir /s /q "%~dp0src\"

pause
FOR /f "usebackq delims=" %%a IN ("%~dp0requirements\dev.txt") DO git clone %%a 

pushd frontend
set PATH=%PATH%;.\node_modules\.bin\
call npm config set "strict-ssl" false
call npm install --no-progress --non-interactive
if errorlevel 1 goto ending
popd