import hashlib

from validate_email import validate_email
from django.shortcuts import render

from time import time

from django.views.generic import DetailView
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

#from src.social.instagram import InstagramService
from src.social.vk.my_vk import get_group_vk_comments, get_commnets_from_post
from src.social.youtube import my_youtube as youtube

from com_train.settings import active_vk_sessions, youtube_session
from .models import UserEmail
from .sender import send
from .serializers.serializers import *
from .tasks import nb_comments


class BaseView(GenericAPIView):
    serializer_class = UserDataSerializer
    permission_classes = ()
    queue = []

    def definition(self, request):
        serializer_class = self.get_serializer_class()
        serializer = serializer_class(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        url_address = serializer.data.get('link')
        request_count = serializer.data.get('request_count')
        service = serializer.data.get('service')
        user_email = serializer.data.get('user_email')

        data = {'url_address': url_address, 'request_count': request_count, 'service': service,
                'email': user_email}

        return data

    @staticmethod
    def context_data(all_comments, normal_count, bad_count, without_photo_video=None):
        return {'all_count': all_comments, 'normal_comm_count': normal_count, 'bad_comm_count': bad_count,
                'all_count_without_photo_video': without_photo_video, 'comments_mood': normal_count > bad_count}

    def process(self, comments):
        without_photo_video = [i[i.find(']') + 3:] for i in comments if i and len(i.split()) > 3]
        bad_count, normal_count, normal_comments, bad_comments = nb_comments(without_photo_video)
        data = self.context_data(len(comments), normal_count, bad_count, len(without_photo_video))
        data.update(normal_comments=normal_comments)
        data.update(bad_comments=bad_comments)
        return data


class EmailPostView(BaseView):

    def post(self, request, *args, **kwargs):
        definition = self.definition(request)
        check_url = definition['url_address'].find("vk.com") or definition['url_address'].find("youtube.com")
        if validate_email(definition['email']) or check_url != -1:
            definition.update(
                slug=definition['service'] + '-' + hashlib.md5(str(int(time())).encode('utf-8')).hexdigest())
            UserEmail.objects.create(**definition)
            send(definition['email'], definition['slug'])
            return Response()
        else:
            return Response({'error': True})


class PersonalDataView(DetailView, BaseView):
    model = UserEmail

    def get(self, request, *args, **kwargs):
        comments = []
        data = {}
        slug = kwargs.get('slug')
        obj = UserEmail.objects.filter(slug=slug)
        if obj.exists():
            obj = obj.first()
            if obj.service == 'vk':
                url_address = obj.url_address.replace("https://vk.com/", "")
                if url_address.find('wall') > 0:
                    url_address = url_address.split('wall')[1]
                    target_id, post_id = url_address.replace('%2Fall', '').split('_')
                    # TODO добавить разные треды для вытаскивания
                    post_comments = get_commnets_from_post(
                        {'id': post_id, 'vk': active_vk_sessions, 'target': target_id})
                    # TODO починить базовый пакет
                    for comment in post_comments:
                        if type(comment) == list:
                            for comment_text in comment:
                                comments.append(comment_text.get('text'))
                        elif type(comment) == dict:
                            for comment_text in comment.get('items'):
                                comments.append(comment_text.get('text'))
                else:
                    comments = get_group_vk_comments(active_vk_sessions, url_address, obj.request_count)

                    data = self.process(comments)

            elif obj.service == 'youtube':
                url_address = obj.url_address.split('v=')[1].lstrip().split('&')[0]
                comments, statistic = youtube.get_comments_statistics_from_youtube(youtube_session, url_address)
            # elif obj.service == 'instagram':
            #     service = InstagramService()
            #     comments = service.execute(obj.url_address, obj.request_count)

            data = self.process(comments)

        return Response(data)


def start(request):
    return render(request, "start/index.html")
