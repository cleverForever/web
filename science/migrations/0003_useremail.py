# Generated by Django 3.2.8 on 2021-12-02 19:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('science', '0002_auto_20211024_2116'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserEmail',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.CharField(max_length=300, verbose_name='Почта')),
                ('slug', models.SlugField(max_length=255, unique=True, verbose_name='URL')),
                ('url_address', models.CharField(max_length=500, verbose_name='Запрашиваемая ссылка')),
                ('request_count', models.IntegerField(verbose_name='Количество запросов')),
                ('service', models.CharField(max_length=15, verbose_name='Сервис')),
            ],
        ),
    ]
