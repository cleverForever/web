from django.core.mail import send_mail

from com_train import settings


def send(user_email, slug):
    if settings.DEBUG:
        url_local = 'http://127.0.0.1:8080/#/order/'
    else:
        url_local = 'http://45.93.200.131/#/order/'
    if slug is not None:
        message = 'Привет, друг! \n\n Ваша ссылка : ' + url_local + slug
    else:
        message = 'Что-то пошло не так:(\n Свяжитесь с администраторами сайта\n\n тел.: хх-хх-хх\n email: xx@gmail.com'
    try:
        send_mail(
            'FeedBack',
            message,
            'emotions8analytics@gmail.com',
            [user_email],
            fail_silently=False,  # При False send_mail вызовет smtplib.SMTPException
        )
        print('Message sent to ', user_email)
    except:
        print('Message send error')

