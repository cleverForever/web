from django.db import models


class Train(models.Model):
    label = models.TextField(verbose_name='label')
    word = models.TextField(verbose_name='word')
    freq = models.TextField(verbose_name='freq')


class TrainClasses(models.Model):
    classes = models.TextField(verbose_name='P(C)')


class UserEmail(models.Model):
    email = models.CharField(max_length=300, verbose_name="Почта")
    slug = models.SlugField(max_length=255, unique=True, db_index=True, verbose_name="URL")
    url_address = models.CharField(max_length=500, verbose_name="Запрашиваемая ссылка")
    request_count = models.IntegerField(verbose_name="Количество запросов")
    service = models.CharField(max_length=15, verbose_name="Сервис")
