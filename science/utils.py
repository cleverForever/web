def is_iter(o):
    try:
        iter(o)
        return True
    except:
        return False


def flatten(lst):
    if is_iter(lst) and not isinstance(lst, str):
        flat = []
        for i in lst:
            if is_iter(i) and not isinstance(i, str):
                flat.extend(flatten(i))
            else:
                flat.append(i)
        return flat
    return [lst]
