from django.urls import path

from . import views

urlpatterns = [
    path('', views.start),
    path('send', views.EmailPostView.as_view(), name='send'),
    path('order/<slug>/', views.PersonalDataView.as_view(), name='order')
]