import json
from collections import defaultdict

from src.train.tokenizer import get_features
from src.train.train import classify

from com_train.celery import app
from science.models import Train, TrainClasses


@app.task
def nb_comments(comments):
    # Список всех фич из всех комментариев
    model = Train.objects.all()
    model_classes = json.loads(TrainClasses.objects.get().classes)
    prob = defaultdict(lambda: 0.0)
    for obj in model:
        prob[obj.label, obj.word] = float(obj.freq)

    bad_count, normal_count = 0, 0
    bad_comments, normal_comments = [], []
    for comment in comments:
        label = classify((model_classes, prob), get_features(comment))
        if label == 'normal':
            normal_count += 1
            normal_comments.append({'text': comment})
        else:
            bad_count += 1
            bad_comments.append({'text': comment})

    return bad_count, normal_count, normal_comments, bad_comments
