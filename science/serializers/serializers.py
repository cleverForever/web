from rest_framework import serializers


class UserDataSerializer(serializers.Serializer):
    link = serializers.CharField(label="Ссылка", required=True)
    request_count = serializers.IntegerField(label="Количество постов", required=True)
    service = serializers.CharField(label="Сервис", required=True)
    user_email = serializers.CharField(label="Почта", required=True)



