import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'com_train.settings')

app = Celery('com_train')
app.config_from_object('django.conf:settings', namespace='CELERY')

# Load tasks modules from all registered Django app configs.
app.autodiscover_tasks()

